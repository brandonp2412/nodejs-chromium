FROM selenium/standalone-chrome:3

RUN curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash \
	&& sudo apt-get install -y nodejs
